import serial
import cv2
import numpy as np

#Crear un kernel("núcleo") de '1' de 8x8 anterior mente era 3*3 pero no era eficiente
kernel=np.ones((8,8),np.uint8)
#Arreglo de rango HSV del color de la banda rango Minimo y Maximo
NegroBajo=np.array([0,0,0])
NegroAlto=np.array([255,255,75]) 
cap = cv2.VideoCapture(1)
num=0
def nothing(x):
   pass


while(1):
   _,frame = cap.read() #Leer un frame
   h1,w1,m1=frame.shape
   hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV) #Convertirlo a espacio de color HSV
   #Deteccion de colores
   MascaraBanda = cv2.inRange(hsv,NegroBajo,NegroAlto)#Asiganamos para filtrar
   MascaraFiltro=MascaraBanda
   ##############################################################################################################APLICAMOS FILTROS Y CONVOLUACIONES#Filtro:Función que depuera determinados datos,para seleccionar lo mejor dentro de un conjunto#Convolución:Operación matemática en la cual tomamos dos señales y producimos una tercera#Se aplica la transformacion: Dilate("Dilatación: Hincha la zona blanca de la máscara")
   MascaraFiltro=cv2.dilate(MascaraFiltro,cv2.getStructuringElement(cv2.MORPH_RECT,(5,5)),iterations = 1)#Se aplica la transformacion: Erode("Erosión:Reducir el tamaño de objetos blancos de la máscara")
   #MascaraFiltro=cv2.erode(MascaraFiltro,cv2.getStructuringElement(cv2.MORPH_RECT,(3,3)),iterations = 1)#Se aplica la transformacion: Closing("")
   #MascaraFiltro = cv2.morphologyEx(MascaraFiltro,cv2.MORPH_CLOSE,kernel)#Se aplica la transformacion: Opening
   MascaraFiltro = cv2.morphologyEx(MascaraFiltro,cv2.MORPH_OPEN,kernel)
   MascaraFiltro=cv2.dilate(MascaraFiltro,cv2.getStructuringElement(cv2.MORPH_RECT,(5,5)),iterations = 1)
   #############################################################################################################
   cv2.imwrite("original.jpg",MascaraFiltro)
   ImagenFiltrada=cv2.imread("original.jpg")
   MascaraObjeto = cv2.inRange(ImagenFiltrada,NegroBajo,NegroAlto)
   cv2.imshow('VideoObjeto',MascaraObjeto)

   cv2.imwrite("imagen.jpg",MascaraObjeto)
   dim=(20,20)
   resized = cv2.resize(MascaraObjeto, dim, interpolation = cv2.INTER_AREA)
   cv2.imwrite('1.jpg',resized)
   

   #-------------------------------------------------------#
   # Localizamos la posicion del objeto
   """
   M = cv2.moments(MascaraObjeto)
   area=M['m00']
   if M['m00']>50000:
      cx = int(M['m10']/M['m00'])
      cy = int(M['m01']/M['m00'])
      opening=cv2.morphologyEx(MascaraObjeto,cv2.MORPH_OPEN,kernel)
      x,y,w,h=cv2.boundingRect(opening)
      if(cx>306 & cx):
         #Imagen[ FilaInicial : FilaFinal , ColumnaInicial : ColumnaFinal]
         ######crop_img = MascaraObjeto[(y):(y+h+15), (x):(x+w+10)]
         ###cv2.imwrite("o"+str(num)+'.jpg',crop_img)
         ######cv2.imshow("cropped", crop_img)
         #Redimenciona la imagen obtenida en tiempo real"Dany was here"
         ###image = cv2.imread("o"+str(num)+'.jpg')
         #r = 100.0 / image.shape[1]
         #dim = (100, int(image.shape[0] * r))
         
         # perform the actual resizing of the image and show it
         resized = cv2.resize(MascaraObjeto, dim, interpolation = cv2.INTER_AREA)
         cv2.imwrite('reducida.jpg',resized)
         cv2.imshow("resized", resized)
         # Mostramos un circulo AZUL en la posicion en la que se encuentra el objeto
         cv2.circle (frame,(cx,cy),20,(255,0,0), 3)
         #print("x = " + str(cx) + ", y = " + str(cy))
         cv2.putText(frame,"cx =" + str(cx)+",""cy ="+str(cy)+")",(cx+5,cy),1,1,(255,0,0),2);
         ############pruebajhgdshjdjhfg
         cv2.putText(frame,"x =" + str(x)+",""y ="+str(y)+")",(x+5,y),1,1,(255,0,0),2);
         cv2.putText(frame,"w =" + str(w)+",""y ="+str(h)+")",(w+5,h),1,1,(255,0,0),2);
   """



    


   #MascaraPieza = cv2.inRange(MascaraFiltro,NegroBajo,NegroAlto)
   #cv2.imshow('Piezas',MascaraPieza) 
   ##############################################################################################################VENTANAS#Ventana que muestra la aplicacion de los filtros
   #cv2.line(frame,(int(w1/2),0),(int(w1/2),h1),(150,200,0),2)  
   cv2.imshow('resultado',MascaraFiltro)  #Ventana que muestra el video original
   cv2.imshow('VideoOriginal',frame)#Ventana que muestra la mascara de la banda
   cv2.imshow('VideoMascaraBanda',MascaraBanda)

   ##############################################################################################################Opciones para salir
   k = cv2.waitKey(5) & 0xFF
   if k == 27:
      break
cv2.destroyAllWindows()
