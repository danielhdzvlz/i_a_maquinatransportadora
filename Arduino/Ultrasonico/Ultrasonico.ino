/*
Sensor de proximidad y al ser inferior a 10cm 
envia un pulso de alarma por  el pin 13

 HC-SR04 conexiones:
  VCC al arduino 5v 
  GND al arduino GND
  Echo al Arduino pin 6 
  Trig al Arduino pin 7
 */
 
#define Pecho 6
#define Ptrig 7
long duracion, distancia;   
 
void setup() {                
  Serial.begin (9600);       // inicializa el puerto seria a 9600 baudios
  pinMode(Pecho, INPUT);     // define el pin 6 como entrada (echo)
  pinMode(Ptrig, OUTPUT);    // define el pin 7 como salida  (triger)
  }
  
void loop() {
  digitalWrite(Ptrig, LOW);
  delayMicroseconds(2);
  digitalWrite(Ptrig, HIGH);   // genera el pulso de triger por 10ms
  delayMicroseconds(10);
  digitalWrite(Ptrig, LOW);
  duracion = pulseIn(Pecho, HIGH);
  distancia = (duracion/2) / 29;            // calcula la distancia en centimetros  
  if (distancia >= 7 || distancia <= 0){  // si la distancia es mayor a 500cm o menor a 0cm 
    Serial.println("0");                  // no mide nada
  }
  else {
    Serial.println(distancia);           // envia el valor de la distancia por el puerto serial
    //Serial.println("cm");              // le coloca a la distancia los centimetros "cm"
  }
  //delay(400);                                // espera 400ms para que se logre ver la distancia en la consola
}
