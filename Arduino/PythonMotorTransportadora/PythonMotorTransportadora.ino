#define MOV_B_UP 'U'    //Izquierda
#define MOV_B_DOWN 'D'   //Derecha
#define MOV_A_UP 'A'    //Izquierda
#define MOV_A_DOWN 'B'   //Derecha
#define MOV_CLOSE 'C'   //Apagado
// Motor A                                // Motor B
int enA =10;  int in1 =9;   int in2 =8;    int enB =5;   int in3 =7;   int in4 =6;
void setup() {
  Serial.begin(9600);  //Iniciamos el puerto 9600 para la Comunicacion con Python por el mismo7   
  pinMode(enA,OUTPUT);  pinMode(enB,OUTPUT);  pinMode(in1,OUTPUT);  pinMode(in2,OUTPUT);  pinMode(in3,OUTPUT);  pinMode(in4,OUTPUT);// Configuracion de pines de salidas
}

void loop() {
  unsigned char comando = 0;
    if(Serial.available()){
      comando = Serial.read();      
      switch( comando ) {
        case MOV_B_UP://Arriba
          AdelanteB();
          break;
        case MOV_B_DOWN://Abajo
          AbajoB();
          break;
         case MOV_CLOSE://Apagado
          ApagarB();
          break;
      }
  }
}
  void AdelanteB(){
    digitalWrite(in1,HIGH);
    digitalWrite(in2,LOW);
    analogWrite(enA,75);
  }
  void AbajoB(){
    digitalWrite(in1,LOW);
    digitalWrite(in2,HIGH);
    analogWrite(enA,75);
  }
  void ApagarB(){
    digitalWrite(in1,LOW);
    digitalWrite(in2,LOW);
    analogWrite(enA,0);
  }  

  
